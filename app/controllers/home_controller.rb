class HomeController < ApplicationController

  def index
    @countries = Country.joins(:vaccination_updates).where(vaccination_updates: {created_at: Date.today.all_day })
      .select(:name, :total_admin, :today_admin, :today_admin_perc_inc, :total_dist, :today_dist, :today_dist_perc_inc)
      .order(:name)
  end
end