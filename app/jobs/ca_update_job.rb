class CaUpdateJob < ApplicationJob
  queue_as :default

  def perform(record, date=nil)
    begin
      Rails.logger.info "Start job for processing Canada tracker updates"
      url = "https://covid19tracker.ca/vaccinationtracker.html"
      args = ['--incognito', '--no-sandbox', '--disable-dev-shm-usage']
      browser = Watir::Browser.new :chrome, headless: true, options: { args: args }
      browser.goto(url)

      total_dist_raw = browser.b(id: "totalVaccinationsDistCanada").wait_until(timeout: 30, &:present?).text
      index = total_dist_raw.length
      if total_dist_raw.include?(' ')
        index = total_dist_raw.index(' ')
      elsif total_dist_raw.include?('(')
        index = total_dist_raw.index('(')
      end
      total_dist = total_dist_raw[0, index]

      total_admin_raw = browser.b(id: "totalVaccinationsCanada").wait_until(timeout: 30, &:present?).text
      index = total_admin_raw.length
      if total_admin_raw.include?(' ')
        index = total_admin_raw.index(' ')
      elsif total_admin_raw.include?('(')
        index = total_admin_raw.index('(')
      end
      total_admin = total_admin_raw[0, index]

      record.total_dist = total_dist.to_i
      record.total_admin = total_admin.to_i

      if date.nil?
        date = Date.today
      else
        CountryUpdate.check_date(record, date)
        date = Date.parse(date)
      end

      yesterday_record = CountryUpdate.find_by(country_id: record.country_id, created_at: (date - 1.day).all_day)
      updated_record =  CountryUpdate.calculate_stats(record, yesterday_record)
      updated_record.save

      browser.close
    rescue Exception => e
      Rails.logger.error e.message
    end
  end

end