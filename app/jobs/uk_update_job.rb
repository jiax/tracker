class UkUpdateJob < ApplicationJob
  queue_as :default

  def perform(record, date=nil)
    require 'open-uri'
    begin
      Rails.logger.info "Start job for processing United Kingdom tracker updates"

      unless date.nil?
        CountryUpdate.check_date(record, date)
      end

      date = date.nil? ? Date.today : Date.parse(date)

      url = "https://www.england.nhs.uk/statistics/wp-content/uploads/sites/2/#{date.year}/#{'%02d' % date.month}/COVID-19-daily-announced-vaccinations-#{date.day}-#{date.strftime("%B")}-#{date.year}.xlsx"
      sheet = Roo::Spreadsheet.open(URI.open(url), extension: :xlsx)
      #IO.copy_stream(URI.open(url), '/home/dev/COVID-19-daily-announced-vaccinations-1-February-2021.xlsx')

      record.total_dist = 0
      record.total_admin = sheet.cell(13, 'G').to_i
      record.first_dose = sheet.cell(13, 'D').to_i
      record.second_dose = sheet.cell(13, 'E').to_i

      yesterday_record = CountryUpdate.find_by(country_id: record.country_id, created_at: (date - 1.day).all_day)

      updated_record =  CountryUpdate.calculate_stats(record, yesterday_record)
      updated_record.save

    rescue Exception => e
      Rails.logger.error e.message
    end
  end

end