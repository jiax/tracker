class UsUpdateJob < ApplicationJob
  queue_as :default

  def perform(record, date)
    begin
      Rails.logger.info "Start job for processing US tracker updates"
      url = "https://covid.cdc.gov/covid-data-tracker/#vaccinations"
      args = ['--incognito', '--no-sandbox', '--disable-dev-shm-usage']
      browser = Watir::Browser.new :chrome, headless: true, options: { args: args }
      browser.goto(url)

      total_dist = browser.h4(text: "Total Doses Delivered").wait_until(timeout: 30, &:present?).parent.children[1].text.delete(',').to_i
      total_admin = browser.h4(text: "Total Doses Administered").wait_until(timeout: 30, &:present?).parent.children[1].text.delete(',').to_i
      first_dose = browser.h4(text: "Number of People Receiving 1 or More Doses").wait_until(timeout: 30, &:present?).parent.children[1].text.delete(',').to_i
      second_dose = browser.h4(text: "Number of People Receiving 2 Doses").wait_until(timeout: 30, &:present?).parent.children[1].text.delete(',').to_i

      record.total_dist = total_dist
      record.total_admin = total_admin
      record.first_dose = first_dose
      record.second_dose = second_dose

      if date.nil?
        date = Date.today
      else
        CountryUpdate.check_date(record, date)
        date = Date.parse(date)
      end

      yesterday_record = CountryUpdate.find_by(country_id: record.country_id, created_at: (date - 1.day).all_day)
      updated_record =  CountryUpdate.calculate_stats(record, yesterday_record)
      updated_record.save

      browser.close
    rescue Exception => e
      Rails.logger.error e.message
    end
  end

end