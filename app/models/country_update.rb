class CountryUpdate < VaccinationUpdate

  def self.create_update_job(country, date=nil)
    begin
      country_id = Country.find_by(name: country).id
      record = CountryUpdate.create!(country_id: country_id)

      case country
        when 'US'
          UsUpdateJob.perform_now(record, date)
        when 'Canada'
          CaUpdateJob.perform_now(record, date)
        when 'UK'
          UkUpdateJob.perform_now(record, date)
        else
          # do nothing
      end
    rescue Exception => e
      Rails.logger.error(e.message)
    end
  end

  # If a VaccinationUpate record should use different created_at and updated_at than the existing timestamps
  def self.check_date(record, date)
    unless date.nil?
      record.created_at = DateTime.parse(date)
      record.updated_at = DateTime.parse(date)
    end
  end

  # Calculate stats such as vaccination rate between yesterday and today's numbers
  def self.calculate_stats(today, yesterday)
    unless yesterday.nil?
      today.today_dist = today.total_dist - yesterday.total_dist
      today.today_dist_perc_inc = yesterday.today_dist.to_i.zero? ? today.today_dist : ((today.today_dist.to_f / yesterday.today_dist.to_f) * 100)
      today.today_admin = today.total_admin - yesterday.total_admin
      today.today_admin_perc_inc = yesterday.today_admin.to_i.zero? ? today.today_admin : ((today.today_admin.to_f / yesterday.today_admin.to_f) * 100)
    end
    today
  end
end