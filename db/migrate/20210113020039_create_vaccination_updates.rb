class CreateVaccinationUpdates < ActiveRecord::Migration[6.1]
  def change
    create_table :vaccination_updates do |t|
      t.integer :country_id
      t.integer :distributed
      t.integer :first_dose
      t.integer :second_dose
      t.timestamps
    end

    add_index :vaccination_updates, :country_id
    add_index :vaccination_updates, :distributed
    add_index :vaccination_updates, :first_dose
    add_index :vaccination_updates, :second_dose
  end
end
