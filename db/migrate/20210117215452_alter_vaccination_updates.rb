class AlterVaccinationUpdates < ActiveRecord::Migration[6.1]
  def change
    remove_index :vaccination_updates, :distributed
    rename_column :vaccination_updates, :distributed, :total_distributed
    add_index :vaccination_updates, :total_distributed

    add_column :vaccination_updates, :total_administered, :integer
  end
end
