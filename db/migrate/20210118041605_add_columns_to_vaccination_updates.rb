class AddColumnsToVaccinationUpdates < ActiveRecord::Migration[6.1]
  def change
    add_column :vaccination_updates, :today_distributed, :integer
    add_column :vaccination_updates, :today_distributed_perc_inc, :decimal, precision: 10, scale: 2
    add_column :vaccination_updates, :today_administered, :integer
    add_column :vaccination_updates, :today_administered_perc_inc, :decimal, precision: 10, scale: 2
  end
end
