class RenameColumnsInVaccinationUpdates < ActiveRecord::Migration[6.1]
  def change
    rename_column :vaccination_updates, :total_distributed, :total_dist
    rename_column :vaccination_updates, :today_distributed, :today_dist
    rename_column :vaccination_updates, :today_distributed_perc_inc, :today_dist_perc_inc
    rename_column :vaccination_updates, :total_administered, :total_admin
    rename_column :vaccination_updates, :today_administered, :today_admin
    rename_column :vaccination_updates, :today_administered_perc_inc, :total_admin_perc_inc
  end
end
