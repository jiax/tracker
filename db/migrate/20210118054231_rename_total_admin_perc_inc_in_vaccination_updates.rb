class RenameTotalAdminPercIncInVaccinationUpdates < ActiveRecord::Migration[6.1]
  def change
    rename_column :vaccination_updates, :total_admin_perc_inc, :today_admin_perc_inc
  end
end
