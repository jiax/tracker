# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_18_054231) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "vaccination_updates", force: :cascade do |t|
    t.integer "country_id"
    t.integer "total_dist"
    t.integer "first_dose"
    t.integer "second_dose"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "total_admin"
    t.integer "today_dist"
    t.decimal "today_dist_perc_inc", precision: 10, scale: 2
    t.integer "today_admin"
    t.decimal "today_admin_perc_inc", precision: 10, scale: 2
    t.index ["country_id"], name: "index_vaccination_updates_on_country_id"
    t.index ["first_dose"], name: "index_vaccination_updates_on_first_dose"
    t.index ["second_dose"], name: "index_vaccination_updates_on_second_dose"
    t.index ["total_dist"], name: "index_vaccination_updates_on_total_dist"
  end

end
