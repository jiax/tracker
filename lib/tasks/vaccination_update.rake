namespace :vaccination do
  desc 'Update vaccination stats'
  task us: :environment do
    CountryUpdate.create_update_job('US')
  end

  task canada: :environment do
    CountryUpdate.create_update_job('Canada')
  end

  task uk: :environment do
    CountryUpdate.create_update_job('UK')
  end
end